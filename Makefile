ENTITY_LABEL="Vincenzo Mannino"
LIMIT=100
user-config.py:
	python core/pwb.py core/generate_user_files.py

runlabel: user-config.py
	python core/pwb.py link_wikidata_cmtq.py --label ${ENTITY_LABEL}

runlimit: user-config.py
	python core/pwb.py link_wikidata_cmtq.py --limit ${LIMIT}

run: user-config.py
	python core/pwb.py link_wikidata_cmtq.py

check:
	mypy link_wikidata_cmtq.py

freeze-requirements:
	pip freeze > requirements.txt
